# Selenium ?
Selenium 은 웹 애플리케이션을 위한 테스팅 프레임워크 입니다. 
자동화 테스트를 위해 여러 가지 기능을 지원합니다. 다양한 언어에서도 사용이 가능합니다. 
Beautiful Soap 는 웹사이트에서 버튼을 클릭해야 얻을 수 있는 데이터라던가, javascript 에 조건이 충족되어야 만 얻을 수 있는 데이터에 접근하는 것에 한계가 있습니다.
그래서, 직접적으로 웬 사이트에 접근할 수 있게 해주는 Selenium을 사용해야 합니다. 
새로운 환경에서 웹 브라우저를 대신해 줄 Web Driver 가 필요합니다. Web Driver 를 눌러 설치를 합니다. Web Driver 는 Selenium 으로 자동화하여 웹 사이트를 탐험하면 됩니다.

# Selenium 이해하기 
pip 명령어를 사용해 Selenium 을 설치
```sh
$ pip3 install selenium
```
Python 파일이나 Jupyter 로 코드를 실행
```py
from selenium import webdriver

path = "Webdriver의 경로를 입력합니다."
driver = webdriver.Chrome(path)
```
Selenium 으로 제어하기 때문에 크롬창에 자동화된 테스트 소프트웨어로 제어중이라는 문구 출력
```py
driver.get('https://www.naver.com')
```
네이버로 접속 됩니다.


# Selenium 으로 검색하기
코드 작성 
```py
from selenium import webdriver

path = "Webdriver 경로를 입력합니다."
driver = webdriver.Chrome(path)
driver.get("http://google.com/")
search_box = driver.find_element_by_name('q')
search_box.send_keys("검색어")
search_box.submit()
```
실행하면 검색어라는 단어로 검색이 되는 것을 볼 수 있습니다.\
```py
search_box = driver.find_element_by_name("q")
```
개발자 도구로 검색어 input box 를 검색해보면 name 이 q 인것을 확인할 수 있습니다.\
search_box 가 커서를 어디다 둬야할 지 name으로 찾아 줍니다.
```py
search_box.send_keys("검색어")
search_box.submit()
```
아까 찾은 검색 input 에 검색어를 입력하고 submit() 으로 검색 버튼을 누릅니다.


# Selenium 기능 정보
명령어 | 기능 
|------|-----|
driver.find_element_by_xpath(" 수만은 기능 ")|코드 패스를 이용한 방법
elemParent.click()|요소 클릭
submit()|서브밋전송
elem.send_keys(Keys.RETURN)|엔터
driver.find_element_by_id|id 검색