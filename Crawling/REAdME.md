# Crawling

# beautiful soup
html 파서
```sh
$ pip3 install beautifulsoup4
```
뷰티풀 수프 홈페이지\
https://www.crummy.com/software/BeautifulSoup/bs4/doc/

# seleninum
1. 설치

<code>
$ pip3 install selenium
</code>

2. 웹 드라이버 다운로드

브라우저 마다 웹 드라이버가 다르다.\
크롬을 이용하여 진행 ( 크롬 버전에 맞게 드라이버 설치 )
https://sites.google.com/a/chromium.org/chromedriver/downloads

driver 폴더를 만들어 폴더 안에 압축을 푼 파일을 넣어준다.

셀레니움 홈페이지 https://www.seleniumhq.org

셀레니움 웹 드라이버 다운로드 https://selenium.dev/downloads