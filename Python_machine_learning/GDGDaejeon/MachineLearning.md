# GDG Daejeon (Machin Learning)


## #Step 1 
### 소개
 - Google Codelab 소개
   > https://codelabs.developers.google.com/ \
   Welcome to Codelabs! \
   Google Developers Codelabs provide a guided, tutorial, hands-on coding experience. \
   Most codelabs will step you through the process of building a small application, or adding a new feature to an existing application. \
   They cover a wide range of topics such as Android Wear, Google Compute Engine, Project Tango, and Google APIs on iOS. \
   Codelab tools on GitHub

- 번역 
   > 코델랩에 온 걸 환영해! \
   구글 개발자 코델랩스는 안내되고 자습적인 코딩 경험을 제공한다. \
   대부분의 코델라브는 작은 응용 프로그램을 만들거나 기존 응용 프로그램에 새로운 기능을 추가하는 과정을 안내할 것이다. \
   안드로이드 웨어, 구글 컴퓨팅 엔진, 프로젝트 탱고, iOS의 구글 API와 같은 다양한 주제를 다룬다. \
   GitHub의 Codelab 도구

- 오늘 학습에 대한 Codelab 주제 
   > https://codelabs.developers.google.com/codelabs/cloud-tensorflow-mnist/#0 \
      TensorFlow, Keras and deep learning, without a PhD \
      

## #Step 2
### Colaboratory  간단한 설명 (코드랩에 등록한 코드를 보고 실습하기 위한 환경 )
   > Jupyter notebook 이나 jupyter lab 같은 환경을 제공하고 구글 문서처럼 공유기능 및 협업을 할 수 있는 환경 제공 \
   바로 바로 업데이트 되며 구글 드라이브, 깃헙, 로컬드라이브에 업로드가 가능하다. \
   > - 런타임설정 \
   ![런타임설정](./colaboratory/Colaboratory_001.png)
   > - 파이선 버전 설정 가능 \
   ![런타임설정](./colaboratory/Colaboratory_002.png)
   > - 기본값은 TPU/ CPU 로 되어있지만 많은양에 연산처리를 위한 GUP 설정을 지원한다. \
   ![런타임설정](./colaboratory/Colaboratory_003.png)
   > - GPU 환경으로 연결 확인가능 \
   ![런타임설정](./colaboratory/Colaboratory_004.png)
   

## #Step 3
   > - 오늘 Codelab 실습에 사용된 코드 \
   코드 링크 클릭시 해당 코드콜라보레이터로 연결 \
   ![codelab001](./codelab/codelab_001.png)

   > - 순차적으로 실행 시켜준다. \
   코드를 한번에 실행하는 것이 아니라  한단락 한단락씩 위에서 실행되면서 결과를 중간 중간 확인 가능한 구조 \
   import 를 한다음 tensorflow 라이브러리 함수들으르 가져다 사용하였다. \
   ![codelab002](./codelab/codelab_002.png)
   
   > - Codelab 에 실습 내용이 적용 된 부분 \
   모델 < 에 학습 방법 및 범위를 설정해주고 \
   다음 블럭에서 학습하여 표로 표현해주는 곳 \ 
   왼쪽 그래프는 손실을 보여주는 것이고 오른쪽은 결과를 보여주는 것인데 \
   어떤 방법을 학습 시키냐에 따라 손실쪽이 엉망이 될 수 있지만 결과는 1 에 가까운 값이 나오기도 하고 복잡했음.. 
   ![codelab003](./codelab/codelab_003.png)


## #Step 4
   > - 공부 할 수 있는 URL \
   ( 구글코드랩 영어 ) 구글에서 아주 잘 단계별로 설명해놨지만 영어라 힘들다.\
    https://codelabs.developers.google.com/?cat=TensorFlow \
    \
   ( 텐센 플로우 한글 ) 텐서 플로우에 대한 내용이 자세히 가이드 되어있다. \
   https://www.tensorflow.org/?hl=ko \
   \
   ( 모델 및 데이터셋 ) 이미 잘 만들어진 모델과 데이터셋을 오픈 제공한다. \
   https://www.tensorflow.org/resources/models-datasets?hl=ko \
   \
   ( 텐서플로우 허브 ) 텐서 성지 \
   https://tfhub.dev/
   

