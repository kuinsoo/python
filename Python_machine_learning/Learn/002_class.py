# Class
""" 
Object-Oriented Language 객체지향 언어

class className:
   def __init__(self, parameter, ...):
      ...
   def 메소드이름1(self, 인수, ...):
      ...
   def 메소드이름2(self, 인수, ...):
      ...

"""
# 클래스 이름은 대문자로 시작
class Man: 
   def __init__(self, name): # 객체생성시 실행
      self.name = name
      print('Initialized!')

   def hello(self):
      print('Hello ' + self.name + '!')

   def goodbye(self):
      print('Good-bye ' + self.name + '!')
   
m = Man('David')
m.hello()
m.goodbye()