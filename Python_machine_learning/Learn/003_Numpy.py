""" 
numpy 사용
"""

import numpy as np

x = np.array([1.0, 2.0, 3.0])
print(x)
print(type(x))


# numpy 배열 산술 연산
y = np.array([2.0, 4.0, 6.0])
print(x+y)
print(x-y)
print(x*y)
print(x/y)


# numpy N 차원 배열
A = np.array([[1,2], [3,4]])
print('A = \n', A)
print(A.shape)
print(A.dtype)

B = np.array([[3,0], [0,6]])
print('B = \n', B)
print('A+B = \n', A+B)
print('A*B = \n', A*B)

print('A*10 = \n', A*10)

# Broad Cast
print(' ========= Broad Cast ========= ')
A = np.array([[1,2], [3,4]])
B = np.array([10,20])
print(A*B)

A = np.array([[1,2], [3,4]])
B = np.array([[10], [5]])
print(A*B)


# 원소접근
print(' ========= Element Access ========= ')
X = np.array([[51, 54], [14, 19], [0,4]])
print(X[0])
print(type(X[0]))
print(X[0][0])
print(type(X[0][0]))

X = X.flatten() # [51,54,14,19,0,4]  2차원으로 만들어 준다.
print(X)
print(type(X))

X1 = X[np.array([0,2,4])]  # X[0] X[2] X[4] 
print(X1)
print(type(X1))

X2 = X>15    # [True False Ture False False]
print('X>15 = ', X2 , 'type = ', type(X2))
X22 = X[X>15]
print('X[X>15] = ', X22 , 'type = ', type(X22))

X3 = X[X2]
print('X[X2] = ', X3 , 'type = ', type(X3))

